export class App {
    menus = null;
    selectedDay = null;
    constructor() {
        this.menus = null;
        loadJSON((data) => {
            const menusData = data;
            this.menus = JSON.parse(menusData);
            this.selectedDay = this.menus[0];
        });
    }
    set(day) {
        this.selectedDay = day;
    }
}

function loadJSON(callback) {
    var xobj = new XMLHttpRequest();
    xobj.overrideMimeType('application/json');
    xobj.open('GET', 'menus.json', true);
    xobj.onreadystatechange = function() {
        if (xobj.readyState == 4 && xobj.status == '200') {
            callback(xobj.responseText);
        }
    };
    xobj.send(null);
}

export class StripYearValueConverter {
    toView(date) {
        if (!date) return '';
        return date.slice(0, -4);
    }
}
